Réunion CA Debian France 2020
=============================

Prévu en Septembre 2020

----------------------------------------

Sujets prévus :

+ Mise à jour RI (finalisation / discussions / vote)
+ Mise à jour du site (contenu / thème / présentations / ...)
+ Gestion financière des Mini-DebConf
+ RGPD
