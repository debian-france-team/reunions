Réunions Debian France
======================

Réunions publiques Debian France

----------------------------------------

## Archives

+ [2023](archives/2023)
+ [2022](archives/2022)
+ [2021](archives/2021)
+ [2020](archives/2020)
+ [2019](archives/2019)

----------------------------------------

# Réunions du CA Debian France

## Septembre 2020

+ [Sujets](CA-reunion-2020.md)
