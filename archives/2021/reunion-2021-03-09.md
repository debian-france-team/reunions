Réunion IRC Debian France
=========================

Prévu le : Mardi 09 Mars 2021 à 21h00

----------------------------------------

# Débats

- Debian Facile et Debian France - Retours après apéro du 13 Février
- Votes CA & Bureau - Retours et avis

----------------------------------------

# Les sujets en cours

- Debian Academy
- Le « Cheat Cube Debian »
- Les Goodies
- planet-debian et planet-debian.org
- L'AG 2021

----------------------------------------

# Compte-rendu

- TODO

----------------------------------------

# Évènements à venir

- JDLL (Lyon) - 03 & 04 avril 2021
- Mini-DebConf Bordeaux - Reportée
- Mobilizon | Meetup ?
