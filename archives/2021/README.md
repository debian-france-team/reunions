Réunions Debian France
======================

# Archives 2021

----------------------------------------

## Réunion IRC 12 Octobre 2021

+ [Sujet](reunion-2021-10-12.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2021/debian-france.2021-10-12-18.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2021/debian-france.2021-10-12-18.59.log.html)

## Réunion IRC 14 Septembre 2021

+ [Sujet](reunion-2021-09-14.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2021/debian-france.2021-09-14-18.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2021/debian-france.2021-09-14-18.59.log.html)

## Réunion IRC 08 Juin 2021 - Annulée

## Réunion IRC 11 Mai 2021

+ [Sujet](reunion-2021-05-11.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2021/debian-france.2021-05-11-19.00.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2021/debian-france.2021-05-11-19.00.log.html)

## Réunion IRC 13 Avril 2021

+ [Sujet](reunion-2021-04-13.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2021/debian-france.2021-04-13-18.58.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2021/debian-france.2021-04-13-18.58.log.html)

## Réunion IRC 09 Mars 2021

+ [Sujet](reunion-2021-03-09.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2021/debian-france.2021-03-09-19.57.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2021/debian-france.2021-03-09-19.57.log.html)

## Réunion IRC 09 Février 2021

+ [Sujet](reunion-2021-02-09.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2021/debian-france.2021-02-09-19.58.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2021/debian-france.2021-02-09-19.58.log.html)

## Réunion IRC 12 Janvier 2021

+ [Sujet](reunion-2021-01-12.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2021/debian-france.2021-01-12-19.57.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2021/debian-france.2021-01-12-19.57.log.html)
