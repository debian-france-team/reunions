Réunion IRC Debian France
=========================

Prévu : 13 Septembre 2019 à 21H00

- [ ] Stickers et Flyers Debian LTS (Freexian) sur le stand ?
- [ ] Qwant Causes : Page Debian France ?
- [ ] Revoir status ?
- [ ] Revoir règlement intérieur ?
- [ ] Debian France et EVL (En Vente Libre)
- [ ] Nettoyage des adhérents (galette)
- [ ] Groupe et « entreprise » Debian France sur LinkedIn
- [ ] Meetup.com - cotisations
- [ ] Position de Debian France avec les autres associations
- [ ] Impression de : Flyer / carte de référence / dfsg
- [ ] Le site : contenu, esthétisme : proposition d'amélioration et d'optimisation
  - [ ] Page « events » toujours d'actu ? Juste laisser évènements ?
  - [ ] Volontaire(s) revue et maj contenue ?
- [ ] Debian France et Salsa
- [ ] Mini-DebConf 2020 à Bordeaux
