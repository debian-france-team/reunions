Réunions Debian France
======================

# Archives 2019

----------------------------------------

## Réunion IRC 11 Décembre 2019

+ [Sujets](archives/2019/reunion-2019-12-11.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2019/debian-france.2019-12-11-20.11.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2019/debian-france.2019-12-11-20.11.log.html)

----------------------------------------

## Réunion IRC 13 Novembre 2019

+ [Sujets](archives/2019/reunion-2019-11-13.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2019/debian-france.2019-11-13-19.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2019/debian-france.2019-11-13-19.59.log.html)

----------------------------------------

## Réunion IRC 09 Octobre 2019

+ [Sujets](archives/2019/reunion-2019-10-09.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2019/debian-france.2019-10-09-18.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2019/debian-france.2019-10-09-18.59.log.html)

----------------------------------------

## Réunion IRC 13 Septembre 2019

+ [Sujets](archives/2019/reunion-2019-09.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2019/debian-france.2019-09-13-19.03.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2019/debian-france.2019-09-13-19.03.log.html)

