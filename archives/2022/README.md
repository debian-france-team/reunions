Réunions Debian France
======================

# Archives 2022

----------------------------------------

## Réunion IRC 13 Décembre 2022

+ [Résumé](http://meetbot.debian.net/debian-france/2022/debian-france.2022-12-13-20.02.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2022/debian-france.2022-12-13-20.02.log.html)

## Réunion IRC 08 Novembre 2022 - Annulée

## Réunion IRC 12 Octobre 2022 - Reportée

## Réunion IRC 13 Septembre 2022 - Repportée

## Réunion IRC 14 Juin 2022

+ [Sujet](reunion-2022-06-14.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2022/debian-france.2022-06-14-19.12.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2022/debian-france.2022-06-14-19.12.log.html)

## Réunion IRC 10 Mai 2022 - Annulée

## Réunion IRC 12 Avril 2022 - Annulée

## Réunion IRC 08 Mars 2022

+ [Sujet](reunion-2022-03-08.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2022/debian-france.2022-03-08-20.04.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2022/debian-france.2022-03-08-20.04.log.html)

## Réunion IRC 08 Février 2022

+ [Résumé](http://meetbot.debian.net/debian-france/2022/debian-france.2022-02-08-20.00.txt)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2022/debian-france.2022-02-08-20.00.log.html)
