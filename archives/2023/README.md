Réunions Debian France
======================

# Archives 2023

----------------------------------------

## Réunion IRC 12 Septembre 2023
+ [Sujet](reunion-2023-09-12.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2023/debian-france.2023-09-12-19.35.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2023/debian-france.2023-09-12-19.35.log.html)

## Réunion IRC 13 Juin 2023
+ [Sujet](reunion-2023-06-13.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2023/debian-france.2023-06-13-19.03.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2023/debian-france.2023-06-13-19.03.log.html)

## Réunion IRC 14 Mars 2023
+ [Résumé](http://meetbot.debian.net/debian-france/2023/debian-france.2023-03-14-20.00.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2023/debian-france.2023-03-14-20.00.log.html)

## Réunion IRC 10 Janvier 2023
+ [Sujet](reunion-2023-01-10.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2023/debian-france.2023-01-10-20.00.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2023/debian-france.2023-01-10-20.00.log.html)
