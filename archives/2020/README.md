Réunions Debian France
======================

# Archives 2020

----------------------------------------

## Réunion IRC 08 Décembre 2020

+ [Sujet](reunion-2020-12-08.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-12-08-19.58.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-12-08-19.58.log.html)

## Réunion IRC 10 Novembre 2020

+ [Sujet](reunion-2020-11-10.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-11-10-20.01.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-11-10-20.01.log.html)

## Réunion IRC 13 Octobre 2020 - Annulée

## Réunion IRC 08 Septembre 2020 - Annulée

## Réunion IRC 9 Juin 2020

+ [Sujet](reunion-2020-06-09.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-06-09-18.58.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-06-09-18.58.log.html)

## Réunion IRC 12 Mai 2020

+ Annulée, seulement deux participants.

## Réunion IRC 14 Avril 2020

+ [Sujet](reunion-2020-04-14.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-04-14-18.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-04-14-18.59.log.html)

## Réunion IRC 10 Mars 2020

+ [Sujet](reunion-2020-03-10.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-03-10-20.00.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-03-10-20.00.log.html)

----------------------------------------

## Réunion IRC 11 Février 2020

+ [Sujet](reunion-2020-02-11.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-02-11-19.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-02-11-19.59.log.html)

----------------------------------------

## Réunion IRC 14 Janvier 2020

+ [Sujet](reunion-2020-01-14.md)
+ [Résumé](http://meetbot.debian.net/debian-france/2020/debian-france.2020-01-14-19.59.html)
+ [Réunion complète](http://meetbot.debian.net/debian-france/2020/debian-france.2020-01-14-19.59.log.html)
